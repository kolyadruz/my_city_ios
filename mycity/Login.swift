//
//  Login.swift
//  mycity
//
//  Created by Nikolay Druzianov on 12/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift
import FirebaseMessaging

class Login: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Messaging.messaging().subscribe(toTopic: "i0")
    }

    override func viewDidAppear(_ animated: Bool) {
        
        login()
        
        
        
    }
    
    func login() {
        
        Interactor().makeRequest(funcName: "login", requestParams: ["fb_token":""], delegate: self) { responseData in
    
            DispatchQueue.main.async {
            
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(LoginResponseData.self, from: responseData)
            
                if let exceptedTopics: [String] = UserDefaults.standard.array(forKey: "exceptedTopics") as? [String] {
                    if exceptedTopics.count == 0 {
                        for category in resp.data.categories {
                            Messaging.messaging().subscribe(toTopic: "\(category.id)")
                        }
                    } else {
                        for category in resp.data.categories {
                            let catIdStr = "\(category.id)"
                            for topic in exceptedTopics {
                                if topic != catIdStr {
                                    Messaging.messaging().subscribe(toTopic: "i\(catIdStr)")
                                }
                            }
                        }
                    }
                }
                
                print(resp)
                
                UserDefaults.standard.set(resp.data.token, forKey: "token")
                
                self.openMain(data: resp.data)
            }
            
        }
        
    }
    
    func openMain(data: MainData) {
        
        let mainPage = self.storyboard?.instantiateViewController(withIdentifier: "MainWithSide") as! SideMenuVC
        mainPage.data = data
        self.present(mainPage, animated: true, completion: nil)
        
    }
    
}
