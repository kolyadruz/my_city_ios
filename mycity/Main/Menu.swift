//
//  Menu.swift
//  mycity
//
//  Created by Nikolay Druzianov on 13/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift

class Menu: UIViewController, SideMenuControllerDelegate {

    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var menuTable: UITableView!
    
    var menuTitles = ["Главная", "Избранное", "Настройки", "Сотрудничество", "Сканировать QR", "О приложении"]
    var menuIconsNames = ["ic_home", "ic_heart", "ic_settings", "ic_barchart", "ic_qr", "ic_info"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuController?.delegate = self
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "Favorites") }, with: "1")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "Settings") }, with: "2")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "Collaboration") }, with: "3")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "QR") }, with: "4")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "About") }, with: "5")
        
        menuTable.selectRow(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .none)
        
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.sideMenuController?.hideMenu()
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
        //Set the default sharing message.
        let message = "Рекомендую скачать приложение Мой город - скидки, акции и события в нашем городе"
        //Set the link to share.
        if let link = NSURL(string: "http://mycity.rvcode.ru") {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}


extension Menu: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuTitles.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let currentCell = cell as! MenuCell
        if indexPath.row == 0 {
            currentCell.icon.tintColor = UIColor(named: "light_blue")
            currentCell.titleLbl.textColor = UIColor(named: "light_blue")
        } else {
            currentCell.icon.tintColor = UIColor(named: "dark_gray")
            currentCell.titleLbl.textColor = UIColor(named: "dark_gray")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        cell.setCell(icon: UIImage(named: self.menuIconsNames[indexPath.row])!, title: self.menuTitles[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuCell
        cell.icon.tintColor = UIColor(named: "light_blue")
        cell.titleLbl.textColor = UIColor(named: "light_blue")
        
        sideMenuController?.setContentViewController(with: "\(indexPath.row)")
        sideMenuController?.hideMenu()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuCell
        cell.icon.tintColor = UIColor(named: "dark_gray")
        cell.titleLbl.textColor = UIColor(named: "dark_gray")
    }
    
}
