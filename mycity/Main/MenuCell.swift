//
//  MenuCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 15/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(icon: UIImage, title: String) {
        
        self.icon.image = icon.withRenderingMode(.alwaysTemplate)
        self.titleLbl.text = title
        
    }

}
