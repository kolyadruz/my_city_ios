//
//  About.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift
import MessageUI

class About: UIViewController, MFMailComposeViewControllerDelegate {

    var contacts: ContactsDetail? {
        didSet {
            if let techEmail = contacts?.emailTech {
                self.techEmailBtn.setTitle(techEmail, for: .normal)
                self.techEmailBtn.layoutIfNeeded()
            }
        }
    }
    @IBOutlet weak var techEmailBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
        
        if let sideMenuVC = self.sideMenuController! as? SideMenuVC {
            self.contacts = sideMenuVC.data?.contacts
        }
    }

    @IBAction func hamburgerTapped(_ sender: UIBarButtonItem) {
        self.sideMenuController?.revealMenu()
    }
    
    @IBAction func techEmailTapped(_ sender: UIButton) {
        if let title = sender.titleLabel?.text {
            sendEmail(recipient: title, subject: "Техпомощь", text: "")
        }
    }
    
    func sendEmail(recipient: String, subject: String, text: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipient])
            mail.setSubject(subject)
            mail.setMessageBody("<p>\(text)</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func termsTapped(_ sender: UIButton) {
        if let url = URL(string: "http://ggls.ru/license") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func policyTapped(_ sender: UIButton) {
        if let url = URL(string: "http://ggls.ru/privacy") {
            UIApplication.shared.open(url)
        }
    }
    
}
