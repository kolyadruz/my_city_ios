//
//  ArticleDetail.swift
//  mycity
//
//  Created by Nikolay Druzianov on 29/06/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class ArticleDetail: UIViewController {

    @IBOutlet weak var blurredImgView: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    var articleDetail: ArticleDetailExtra?
    
    var articleId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getArticle(self.articleId!)
    }
    
    func getArticle(_ id: Int) {
        Interactor().makeRequest(funcName: "getarticle", requestParams: ["articleId":"\(id)"], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(ArticleDetailResponseData.self, from: responseData)
                
                self.articleDetail = resp.data.articleData
                
                self.blurredImgView.sd_setImage(with: URL(string: self.articleDetail!.image))
                self.imgView.sd_setImage(with: URL(string: self.articleDetail!.image))
            
                self.titleLbl.text = self.articleDetail!.title
                self.viewsLbl.text = "\(self.articleDetail!.views)"
                self.descriptionLbl.attributedText = self.articleDetail!.body.htmlToAttributedString
                self.descriptionLbl.layoutIfNeeded()
                
            }
            
        }
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func likeTapped(_ sender: UIButton) {
        var funcName = ""
        
        if self.articleDetail?.isFavorite == 1 {
            funcName = "articleremovefav"
        } else {
            funcName = "articletofav"
        }
        
        Interactor().makeRequest(funcName: funcName, requestParams: ["articleId":"\(self.articleDetail!.id)"], delegate: UIViewController()) { responseData in
            
            DispatchQueue.main.async {
                let decoder = JSONDecoder()
                print(responseData)
                let resp = try! decoder.decode(ArticleToFavResponseData.self, from: responseData)
                
                if resp.data.err_id == 0 {
                    if let liked = self.articleDetail?.isFavorite {
                        if liked == 1 {
                            self.articleDetail?.isFavorite = 0
                            self.likeBtn.setImage(UIImage(named: "ic_large_heart"), for: .normal)
                        } else {
                            self.articleDetail?.isFavorite = 1
                            self.likeBtn.setImage(UIImage(named: "ic_large_heart_active"), for: .normal)
                        }
                    }
                }
            }
            
        }
    }
    
}
