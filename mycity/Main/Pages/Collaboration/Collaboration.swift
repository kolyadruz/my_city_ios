//
//  Collaboration.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import MessageUI
import SideMenuSwift

class Collaboration: UIViewController, MFMailComposeViewControllerDelegate {

    var contacts: ContactsDetail? {
        didSet {
            
            if let email = contacts?.email {
                self.emailBtn.setTitle(email, for: .normal)
                self.emailBtn.layoutIfNeeded()
            }
            if let techEmail = contacts?.emailTech {
                self.techEmailBtn.setTitle(techEmail, for: .normal)
                self.techEmailBtn.layoutIfNeeded()
            }
            if let phone = contacts?.phoneCall {
                self.infoLbl.text = "Чтобы разместить объявление, позвоните по номеру \(phone) или напишите на Email"
            }
            
        }
    }
    
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var techEmailBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
        
        if let sideMenuVC = self.sideMenuController! as? SideMenuVC {
            self.contacts = sideMenuVC.data?.contacts
        }
    }

    @IBAction func hamburgerTapped(_ sender: UIBarButtonItem) {
        self.sideMenuController?.revealMenu()
    }
    
    @IBAction func emailTapped(_ sender: UIButton) {
        if let title = sender.titleLabel?.text {
            sendEmail(recipient: title, subject: "Заявка", text: "Здравстуйте! Хочу разместить рекламу в вашем приложении.")
        }
    }
    
    @IBAction func techEmailTapped(_ sender: UIButton) {
        if let title = sender.titleLabel?.text {
            sendEmail(recipient: title, subject: "Техпомощь", text: "")
        }
    }
    
    func sendEmail(recipient: String, subject: String, text: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipient])
            mail.setSubject(subject)
            mail.setMessageBody("<p>\(text)</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func callTapped(_ sender: UIButton) {
        if let phone = self.contacts?.phoneCall {
            guard let number = URL(string: "tel://+\(phone)") else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @IBAction func instaTapped(_ sender: UIButton) {
        if let insta = self.contacts?.instagram {
            let instagramHooks = "instagram://user?username=\(insta)"
            let instagramUrl = URL(string: instagramHooks)
            if UIApplication.shared.canOpenURL(instagramUrl!) {
                UIApplication.shared.open(instagramUrl!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.open(URL(string: "https://instagram.com/\(insta)")!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func waTapped(_ sender: UIButton) {
        if let wa = self.contacts?.phoneWa {
            let originalString = "Здравствуйте! Хочу разместить рекламу в вашем приложении."
            let escapedString = originalString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
            
            let url  = URL(string: "whatsapp://send?text=\(escapedString!)&phone=\(wa)&abid=\(wa)")
            
            if UIApplication.shared.canOpenURL(url! as URL) {
                UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    
}
