//
//  Favorites.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift

class Favorites: UIViewController {
    
    private let postsItemCellID = "postsItemCell"
    
    var posts: [PublicationDetail]?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var listIsEmptyLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.register(UINib.init(nibName: "PostsItemCell", bundle: nil), forCellWithReuseIdentifier: postsItemCellID)
        
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFavoritePosts()
    }
 
    func getFavoritePosts() {
        
        self.listIsEmptyLbl.isHidden = true
        
        Interactor().makeRequest(funcName: "favorites", requestParams: [:], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(FavoritesResponseData.self, from: responseData)
                
                print(resp)
                
                self.posts = resp.data.data
                
                self.collectionView.reloadData()
                
                if self.posts?.count == 0 {
                    self.listIsEmptyLbl.isHidden = false
                }
            }
            
        }
        
    }
    
    @IBAction func hamburgerTapped(_ sender: UIBarButtonItem) {
        self.sideMenuController?.revealMenu()
    }

}

extension Favorites: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.posts?.count {
            if count == 0 {
                showListIsEmptyLbl()
            } else {
                hideListIsEmptyLbl()
            }
            return count
        } else {
            showListIsEmptyLbl()
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let postsItem = collectionView.dequeueReusableCell(withReuseIdentifier: postsItemCellID, for: indexPath) as! PostsItemCell
        postsItem.setCell(post: self.posts![indexPath.item])
        
        return postsItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.width / 1.31)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func showListIsEmptyLbl() {
        self.collectionView.isHidden = true
        self.listIsEmptyLbl.isHidden = false
    }
    
    func hideListIsEmptyLbl() {
        self.collectionView.isHidden = false
        self.listIsEmptyLbl.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let id = self.posts![indexPath.item].id
        let postDetailView = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail") as! PostDetail
        postDetailView.postId = id
        self.present(postDetailView, animated: true, completion: nil)
        
    }
    
}
