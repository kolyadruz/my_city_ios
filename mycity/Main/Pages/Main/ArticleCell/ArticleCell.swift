//
//  ArticleCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 15/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class ArticleCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var main: Main?
    var articles: [PublicationDetail]?
    
    private let cellID = "articleItem"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Bold.ttf", size: 16)
        label.text = "Интересное в Якутске"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bannersCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        //layout.minimumLineSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    func setupViews() {
        addSubview(bannersCollectionView)
        addSubview(nameLabel)
        
        bannersCollectionView.dataSource = self
        bannersCollectionView.delegate = self
        
        bannersCollectionView.register(ArticleItemCell.self, forCellWithReuseIdentifier: cellID)
        bannersCollectionView.showsHorizontalScrollIndicator = false
        bannersCollectionView.showsVerticalScrollIndicator = false
        //bannersCollectionView.isPagingEnabled = true
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": nameLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": bannersCollectionView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[nameLabel(30)][v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": bannersCollectionView, "nameLabel": nameLabel]))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let article = articles?[indexPath.item] else { return }
        main?.showArticleDetail(article.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = articles?.count {
            
            print("Count of Articles: \(count)")
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! ArticleItemCell
        cell.article = articles?[indexPath.item]
        
        /*if indexPath.item % 2 == 0 {
         cell.backgroundColor = UIColor.red
         } else {
         cell.backgroundColor = UIColor.green
         }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = frame.width / 2.52
        return CGSize(width: width, height: width / 1.49)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = frame.width / 37.5
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let inset = frame.width / 37.5
        return inset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        let inset = frame.width / 37.5
        return inset
    }
    
}
