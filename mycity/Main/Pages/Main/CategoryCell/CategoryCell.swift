//
//  CategoryCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 13/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var main: Main?
    var delegate: CategoryItemToMainContainerDelegate?
    
    var categories: [CategoryDetail]? {
        didSet {
            
            categoriesCollectionView.reloadData()
            
        }
    }
    
    private let cellID = "categoryPage"
    
    var velocityX = CGFloat(0.0)
    var scrollPaddings: CGFloat = 70.0
    
    var pageWidth: CGFloat?
    var pageHeight: CGFloat?
    var collectionViewInset: CGFloat?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionViewInset = frame.width / 49.2
        pageWidth = frame.width - collectionViewInset! * 4
        pageHeight = frame.height - collectionViewInset! * 4
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let categoriesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        //layout.minimumLineSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    let pageControl: UIPageControl = {
        let control = UIPageControl()
        
        control.currentPage = 0
        control.pageIndicatorTintColor = UIColor.init(named: "dots_gray")
        control.currentPageIndicatorTintColor = UIColor.purple
        
        control.translatesAutoresizingMaskIntoConstraints = false
        
        return control
    }()
    
    func setupViews() {
        addSubview(categoriesCollectionView)
        addSubview(pageControl)
        
        categoriesCollectionView.dataSource = self
        categoriesCollectionView.delegate = self
        
        categoriesCollectionView.register(CategoryPageCell.self, forCellWithReuseIdentifier: cellID)
        categoriesCollectionView.showsHorizontalScrollIndicator = false
        categoriesCollectionView.showsVerticalScrollIndicator = false
        //categoriesCollectionView.isPagingEnabled = true
        
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": pageControl]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": categoriesCollectionView]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0][v1(0.5)]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": categoriesCollectionView, "v1": pageControl]))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("items count is: \(self.categories!.count)")
        print("pages count is: \(Int(ceil(Double(self.categories!.count) / 12)))")
        
        if let count = self.categories?.count {
            return Int(ceil(Double(count) / 12))
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let startItemIndex = indexPath.item * 12
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CategoryPageCell
        
        print("number of pages in collection: \(collectionView.numberOfItems(inSection: 0))")
        print("index of last page in collection: \(collectionView.numberOfItems(inSection: 0) - 1)")
        
        if startItemIndex + 11 < self.categories!.endIndex - 1 {
            
            print(self.categories![startItemIndex...startItemIndex + 11])
            
            cell.categories = Array(self.categories![startItemIndex...startItemIndex + 11])
            
        } else {
            
            print(self.categories![startItemIndex...self.categories!.endIndex - 1])
            cell.categories = Array(self.categories![startItemIndex...self.categories!.endIndex - 1])
        }
        
        cell.delegate = self.delegate
        
        print("returned")
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: pageWidth!, height: pageHeight!)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: collectionViewInset! * 2, bottom: 0, right: collectionViewInset! * 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionViewInset!
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        scrollView.isUserInteractionEnabled = false
        self.setContentOffset(scrollView: scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollView.isUserInteractionEnabled = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else {
            return
        }
        
        self.setContentOffset(scrollView: scrollView)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        self.velocityX = velocity.x
    }
    
    func setContentOffset(scrollView: UIScrollView) {
        
        let numOfItems = self.categoriesCollectionView.numberOfItems(inSection: 0)
        let stopOver = scrollView.contentSize.width / CGFloat(numOfItems) - (self.collectionViewInset! * 2) / CGFloat(numOfItems)
        var x = round((scrollView.contentOffset.x + (velocityX*50)) / stopOver) * stopOver
        
        x = max(0, min(x, scrollView.contentSize.width - scrollView.frame.width))
        
        UIView.animate(withDuration: 0.12, animations: {
            scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: false)
        })
    }
    
}
