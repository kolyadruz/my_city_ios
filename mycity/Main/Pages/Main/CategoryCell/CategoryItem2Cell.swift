//
//  CategoryItem2Cell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 30/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class CategoryItem2Cell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countlbl: UILabel!

    var category: CategoryDetail? {
        didSet {
            if let image = category?.image {
                self.imgView.sd_setImage(with: URL(string: image))
            }
            if let title = category?.name {
                self.titleLbl.text = title
            }
            if let notViewed = category?.notViewed {
                if notViewed > 0 {
                    self.countlbl.text = "\(notViewed)"
                } else {
                    self.countView.isHidden = true
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func draw(_ rect: CGRect) {
        self.countView.layer.cornerRadius = self.countView.frame.width / 2
        layer.cornerRadius = frame.width / 11.6
    }
    
}
