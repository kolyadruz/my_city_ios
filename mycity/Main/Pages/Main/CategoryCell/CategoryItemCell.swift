//
//  CategoryItemCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 13/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class CategoryItemCell: UICollectionViewCell {
    
    var category: CategoryDetail? {
        didSet {
            if let image = category?.image {
                imageView.sd_setImage(with: URL(string: image))
            }
            if let notViewed = category?.notViewed {
                
            }
            if let name = category?.name {
                
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.layer.masksToBounds = true
        return iv
    }()
    
    func setupViews() {
        addSubview(imageView)
        
        backgroundColor = UIColor.white
        
        let imgSize = frame.width / 1.8
        let imgInset = (frame.width - imgSize) / 2
        
        imageView.frame = CGRect(x: imgInset, y: imgInset, width: imgSize, height: imgSize)
        imageView.backgroundColor = UIColor(named: "cg_gray")
        layer.cornerRadius = frame.width / 11.6
    }
    
    
}
