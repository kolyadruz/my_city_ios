//
//  CategoryPosts.swift
//  mycity
//
//  Created by Nikolay Druzianov on 16/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class CategoryPosts: UIViewController {

    var id: Int?
    var navTitle: String?
    
    var postIdNeedToBeOpened: Int?
    
    private let bannerCellID = "bannerCell"
    private let postsItemCellID = "postsItemCell"
    
    var banners: [PublicationDetail]?
    var posts: [PublicationDetail]?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.navTitle!
        
        self.collectionView.register(BannerCell.self, forCellWithReuseIdentifier: bannerCellID)
        self.collectionView.register(UINib.init(nibName: "PostsItemCell", bundle: nil), forCellWithReuseIdentifier: postsItemCellID)
        self.collectionView.delaysContentTouches = false
        
        if self.postIdNeedToBeOpened != nil {
            self.showTopBannerDetail(self.postIdNeedToBeOpened!)
            return
        }
        
        getCategoryPosts()
    }
    
    func getCategoryPosts() {
        
        Interactor().makeRequest(funcName: "category", requestParams: ["categoryId":"\(self.id!)"], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(CategoryResponseData.self, from: responseData)
                
                print(resp)
                
                self.posts = resp.data.posts
                
                self.collectionView.reloadData()
            }
            
        }
        
    }
    
    func showTopBannerDetail(_ id: Int) {
        self.postIdNeedToBeOpened = nil
        let postDetailView = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail") as! PostDetail
        postDetailView.postId = id
        self.present(postDetailView, animated: true, completion: nil)
    }

}

extension CategoryPosts: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            if let count = self.posts?.count {
                return count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let bannerCell = collectionView.dequeueReusableCell(withReuseIdentifier: bannerCellID, for: indexPath) as! BannerCell
            bannerCell.banners = self.banners
            bannerCell.categoryPosts = self
            
            return bannerCell
        case 1:
            let postsItem = collectionView.dequeueReusableCell(withReuseIdentifier: postsItemCellID, for: indexPath) as! PostsItemCell
            postsItem.setCell(post: self.posts![indexPath.item])
            
            return postsItem
        default:
            return UICollectionViewCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let bannersHeight = view.frame.width / 3.26
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: bannersHeight)
        case 1:
            return CGSize(width: view.frame.width, height: view.frame.width / 1.31)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var id = 0
        
        switch indexPath.section {
        default:
            id = self.posts![indexPath.item].id
        }
        
        let postDetailView = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail") as! PostDetail
        postDetailView.postId = id
        self.present(postDetailView, animated: true, completion: nil)
        
    }
    
}
