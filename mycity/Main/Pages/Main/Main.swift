//
//  Main.swift
//  mycity
//
//  Created by Nikolay Druzianov on 12/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift

protocol OpenFromNotification {
    func openCategory(category: Int, post: Int)
}

protocol CategoryItemToMainContainerDelegate {
    func openCategory(categoryDetail: CategoryDetail)
}

class Main: UIViewController, CategoryItemToMainContainerDelegate, OpenFromNotification {
    
    
    //TOP BANNERS
    var banners: [PublicationDetail]?
    private let bannerCellID = "bannerItem"
    @IBOutlet weak var topBannersCollection: UICollectionView!
    @IBOutlet weak var topBannersPageControl: UIPageControl!
    var bannerWidth: CGFloat?
    var topBannersCollectionViewInset: CGFloat?
    var timer: Timer?
    var velocityX = CGFloat(0.0)
    var scrollPaddings: CGFloat = 70.0
    
    //CATEGORIES
    private let categoryCellID = "categoryPage"
    var categories: [CategoryDetail]?
    @IBOutlet weak var categoriesCollection: UICollectionView!
    @IBOutlet weak var categoriesPageControl: UIPageControl!
    var categoriesVelocityX = CGFloat(0.0)
    var categoriesScrollPaddings: CGFloat = 70.0
    
    var categoriesPageWidth: CGFloat?
    var categoriesPageHeight: CGFloat?
    var categoriesCollectionViewInset: CGFloat?
    
    //ARTICLES
    private let articleCellID = "articleItem"
    var articles: [PublicationDetail]?
    @IBOutlet weak var articlesCollection: UICollectionView!
    
    var postIdNeedToBeOpened: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mainContainer = self.parent as? MainContainer {
            mainContainer.main = self
        }
        
        setupCollections()
    
    }

    override func viewDidAppear(_ animated: Bool) {
        reloadCounts()
    }
    
    func setupCollections() {
        
        //TOP BANNERS
        let topCollectionLayout = UICollectionViewFlowLayout()
        topCollectionLayout.scrollDirection = .horizontal
        topCollectionLayout.minimumLineSpacing = 0
        
        self.topBannersCollection.collectionViewLayout = topCollectionLayout
        self.topBannersCollection.backgroundColor = .clear
        self.topBannersCollection.translatesAutoresizingMaskIntoConstraints = false
        self.topBannersCollection.delaysContentTouches = false
        self.topBannersCollection.register(BannerItemCell.self, forCellWithReuseIdentifier: bannerCellID)
        self.topBannersCollection.showsHorizontalScrollIndicator = false
        self.topBannersCollection.showsVerticalScrollIndicator = false
        
        self.bannerWidth = self.topBannersCollection.frame.width / 1.12
        self.topBannersCollectionViewInset = (self.topBannersCollection.frame.width - self.bannerWidth!) / 4
        
        self.topBannersPageControl.currentPage = 0
        self.topBannersPageControl.pageIndicatorTintColor = UIColor.init(named: "dots_gray")
        self.topBannersPageControl.currentPageIndicatorTintColor = UIColor.init(named: "dots_gray")
        self.resetTimer()
        
        //CATEGORIES
        let categoriesCollectionLayout = UICollectionViewFlowLayout()
        categoriesCollectionLayout.scrollDirection = .horizontal
        categoriesCollectionLayout.minimumLineSpacing = 0
        
        
        self.categoriesCollection.collectionViewLayout = categoriesCollectionLayout
        self.categoriesCollection.delaysContentTouches = false
        self.categoriesCollection.register(CategoryPageCell.self, forCellWithReuseIdentifier: categoryCellID)
        self.categoriesCollection.showsHorizontalScrollIndicator = false
        self.categoriesCollection.showsVerticalScrollIndicator = false
    
        self.categoriesCollectionViewInset = self.categoriesCollection.frame.width / 49.2
        self.categoriesPageWidth = self.categoriesCollection.frame.width - self.categoriesCollectionViewInset! * 4
        self.categoriesPageHeight = self.categoriesCollection.frame.height
        
        self.categoriesPageControl.currentPage = 0
        self.categoriesPageControl.pageIndicatorTintColor = UIColor.init(named: "dots_gray")
        self.categoriesPageControl.currentPageIndicatorTintColor = UIColor.init(named: "dots_gray")
        
        //ARTICLES
        let artilesCollectionLayout = UICollectionViewFlowLayout()
        artilesCollectionLayout.scrollDirection = .horizontal
        artilesCollectionLayout.minimumLineSpacing = 0
        
        self.articlesCollection.collectionViewLayout = artilesCollectionLayout
        self.articlesCollection.delaysContentTouches = false
        self.articlesCollection.register(ArticleItemCell.self, forCellWithReuseIdentifier: articleCellID)
        
        self.articlesCollection.showsHorizontalScrollIndicator = false
        self.articlesCollection.showsVerticalScrollIndicator = false
        
    }
    
    func reloadCounts() {
        
        Interactor().makeRequest(funcName: "login", requestParams: ["fb_token":""], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(LoginResponseData.self, from: responseData)
                
                self.categories = resp.data.categories
                self.categoriesCollection.reloadData()
                print(resp)
                
                UserDefaults.standard.set(resp.data.token, forKey: "token")
            
            }
            
        }
        
    }
    
    func resetTimer() {
        
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
    }
    
    @objc func moveToNextPage (){
        
        let bannerWidth = self.topBannersCollection.frame.width - self.topBannersCollectionViewInset! * 2
        
        let pageWidth:CGFloat = bannerWidth + self.topBannersCollectionViewInset! / 2
        let maxWidth:CGFloat = pageWidth * CGFloat(self.banners!.count)
        let contentOffset:CGFloat = self.topBannersCollection.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  round(contentOffset + pageWidth) >= round(maxWidth - pageWidth)
        {
            slideToX = 0
        }
        
        topBannersPageControlSetPage(currentOffset: slideToX)
        
        self.topBannersCollection.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.topBannersCollection.frame.height), animated: true)
    }
    
    func topBannersPageControlSetPage(currentOffset: CGFloat) {
        
        let bannerWidth = self.topBannersCollection.frame.width - self.topBannersCollectionViewInset! * 2
        
        let pageWidth:CGFloat = bannerWidth + self.topBannersCollectionViewInset! / 2
        
        let index = Int(currentOffset / pageWidth)
        
        self.topBannersPageControl.currentPage = Int(index)
        
    }
    
    func openCategory(category: Int, post: Int) {
        if post != 0 {
            self.postIdNeedToBeOpened = post
        }
        
        for categoryDetail in categories! {
            if categoryDetail.id == category {
                self.openCategory(categoryDetail: categoryDetail)
            }
        }
    }
    
    func openCategory(categoryDetail: CategoryDetail) {
        let id = categoryDetail.id
        let navTitle = categoryDetail.name
        
        let categoryPosts = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPosts") as! CategoryPosts
        
        categoryPosts.banners = self.banners
        categoryPosts.id = id
        categoryPosts.navTitle = navTitle
        
        if self.postIdNeedToBeOpened != nil {
            categoryPosts.postIdNeedToBeOpened = self.postIdNeedToBeOpened
            self.postIdNeedToBeOpened = nil
        }
        
        self.navigationController?.pushViewController(categoryPosts, animated: true)
    }
    
    func showTopBannerDetail(_ id: Int) {
        let postDetailView = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail") as! PostDetail
        postDetailView.postId = id
        self.present(postDetailView, animated: true, completion: nil)
    }
    
    func showArticleDetail(_ id: Int) {
        let articleDetailView = self.storyboard?.instantiateViewController(withIdentifier: "ArticleDetail") as! ArticleDetail
        articleDetailView.articleId = id
        self.present(articleDetailView, animated: true, completion: nil)
    }
    
}

extension Main: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case self.topBannersCollection:
            
            guard let banner = banners?[indexPath.item] else { return }
            showTopBannerDetail(banner.id)
            
        case self.categoriesCollection:
            
            print("")
            
        case self.articlesCollection:
            
            guard let article = articles?[indexPath.item] else { return }
            showArticleDetail(article.id)
            
        default:
            
            print("")
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
            case self.topBannersCollection:
            
                if let count = banners?.count {
                    
                    self.topBannersPageControl.numberOfPages = count
                    
                    print("Count of Banners: \(count)")
                    return count
                }
                return 0
            
            case self.categoriesCollection:
            
                if let count = self.categories?.count {
                    return Int(ceil(Double(count) / 12))
                } else {
                    return 0
                }
            
            case self.articlesCollection:
            
                if let count = articles?.count {
                    
                    print("Count of Articles: \(count)")
                    return count
                }
                return 0
            
            default:
                return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
            case self.topBannersCollection:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bannerCellID, for: indexPath) as! BannerItemCell
                cell.banner = banners?[indexPath.item]
                
                return cell
            
            case self.categoriesCollection:
                let startItemIndex = indexPath.item * 12
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryCellID, for: indexPath) as! CategoryPageCell
                
                print("number of pages in collection: \(collectionView.numberOfItems(inSection: 0))")
                print("index of last page in collection: \(collectionView.numberOfItems(inSection: 0) - 1)")
                
                if startItemIndex + 11 < self.categories!.endIndex - 1 {
                    
                    print(self.categories![startItemIndex...startItemIndex + 11])
                    
                    cell.categories = Array(self.categories![startItemIndex...startItemIndex + 11])
                    
                } else {
                    
                    print(self.categories![startItemIndex...self.categories!.endIndex - 1])
                    cell.categories = Array(self.categories![startItemIndex...self.categories!.endIndex - 1])
                }
                
                cell.delegate = self
                
                print("returned")
                
                return cell
            case self.articlesCollection:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: articleCellID, for: indexPath) as! ArticleItemCell
                cell.article = articles?[indexPath.item]
                
                return cell
            
            default:
                return UICollectionViewCell()
            }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //guard let navBarHeight = self.navigationController?.navigationBar.frame.height else { return CGSize(width: 0, height: 0) }
        //let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        
        switch collectionView {
        case self.topBannersCollection:
            
            return CGSize(width: bannerWidth!, height: bannerWidth! / 3.67)
            
        case self.categoriesCollection:
            
            return CGSize(width: self.categoriesPageWidth!, height: categoriesPageHeight!)
        
        case self.articlesCollection:
            
            let width = self.articlesCollection.frame.width / 2.52
            return CGSize(width: width, height: width / 1.49)
            
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch collectionView {
        case self.topBannersCollection:
            
            return UIEdgeInsets(top: 0, left: topBannersCollectionViewInset! * 2, bottom: 0, right: topBannersCollectionViewInset! * 2)
            
        case self.categoriesCollection:
            
            return UIEdgeInsets(top: 0, left: categoriesCollectionViewInset! * 2, bottom: 0, right: categoriesCollectionViewInset! * 2)
            
        case self.articlesCollection:
            
            let inset = self.articlesCollection.frame.width / 37.5
            return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
            
        default:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right:  0)
        }
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        switch collectionView {
        case self.topBannersCollection:
            
            return topBannersCollectionViewInset!
            
        case self.categoriesCollection:
            
            return categoriesCollectionViewInset!
            
        case self.articlesCollection:
            
            let inset = self.articlesCollection.frame.width / 37.5
            return inset
            
        default:
            return topBannersCollectionViewInset!
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView {
            case self.articlesCollection:
                let inset = self.articlesCollection.frame.width / 37.5
                return inset
            default:
                return 0
        }
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
        switch scrollView {
        case self.topBannersCollection:
            
            scrollView.isUserInteractionEnabled = false
            self.setContentOffset(scrollView: scrollView)
            
        case self.categoriesCollection:
            
            scrollView.isUserInteractionEnabled = false
            self.setContentOffset(scrollView: scrollView)
            
        case self.articlesCollection:
            
            print("")
            
        default:
            print("")
        }
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollView.isUserInteractionEnabled = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else {
            return
        }
        
        self.resetTimer()
        self.setContentOffset(scrollView: scrollView)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        self.velocityX = velocity.x
        topBannersPageControlSetPage(currentOffset: targetContentOffset.pointee.x)
        
        /*let x = targetContentOffset.pointee.x
        
        topBannersPageControl.currentPage = Int(x / self.frame.width)*/
        
        
        // on each dot, call the transform of scale 1 to restore the scale of previously selected dot
        
        topBannersPageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
        
        // transform the scale of the current subview dot, adjust the scale as required, but bigger the scale value, the downward the dots goes from its centre.
        // You can adjust the centre anchor of the selected dot to keep it in place approximately.
        
        let centreBeforeScaling = self.topBannersPageControl.subviews[self.topBannersPageControl.currentPage].center
        
        self.topBannersPageControl.subviews[self.topBannersPageControl.currentPage].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        
        // Reposition using autolayout
        
        self.topBannersPageControl.subviews[self.topBannersPageControl.currentPage].translatesAutoresizingMaskIntoConstraints = false
        
        self.topBannersPageControl.subviews[self.topBannersPageControl.currentPage].centerYAnchor.constraint(equalTo: self.topBannersPageControl.subviews[0].centerYAnchor , constant: 0)
        
        self.topBannersPageControl.subviews[self.topBannersPageControl.currentPage].centerXAnchor.constraint(equalTo: self.topBannersPageControl.subviews[0].centerXAnchor , constant: 0)
        
        
        //    self.pageControl.subviews[self.pageControl.currentPage].layer.anchorPoint = centreBeforeScaling
        
        
    }
    
    func setContentOffset(scrollView: UIScrollView) {
        
        switch scrollView {
        case self.topBannersCollection:
            
            let numOfItems = self.banners!.count
            let stopOver = scrollView.contentSize.width / CGFloat(numOfItems) - (self.topBannersCollectionViewInset! * 2) / CGFloat(numOfItems)
            var x = round((scrollView.contentOffset.x + (velocityX*50)) / stopOver) * stopOver
            
            x = max(0, min(x, scrollView.contentSize.width - scrollView.frame.width))
            
            UIView.animate(withDuration: 0.12, animations: {
                scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: false)
            })
            
        case self.categoriesCollection:
            
            let numOfItems = self.categoriesCollection.numberOfItems(inSection: 0)
            let stopOver = scrollView.contentSize.width / CGFloat(numOfItems) - (self.categoriesCollectionViewInset! * 2) / CGFloat(numOfItems)
            var x = round((scrollView.contentOffset.x + (velocityX*50)) / stopOver) * stopOver
            
            x = max(0, min(x, scrollView.contentSize.width - scrollView.frame.width))
            
            UIView.animate(withDuration: 0.12, animations: {
                scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: false)
            })
            
        case self.articlesCollection:
            
            print("")
            
        default:
            print("")
        }
        
        
    }
    
}
