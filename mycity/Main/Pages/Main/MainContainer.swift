//
//  MainContainer.swift
//  mycity
//
//  Created by Nikolay Druzianov on 16/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

protocol SearchToMainContainerDelegate {
    func hideSearchView()
}

class MainContainer: UIViewController, SearchToMainContainerDelegate {

    var banners: [PublicationDetail]?
    var categories: [CategoryDetail]?
    var articles: [PublicationDetail]?
    
    var isSearchShown: Bool = false
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    var main: OpenFromNotification?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "logo_header")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
        
    }
    
    func openCategory(category: Int, post: Int) {
        
        hideSearchView()
        self.main?.openCategory(category: category, post: post)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MainSegue" {
            let main = segue.destination as! Main
            main.banners = self.banners
            main.categories = self.categories
            main.articles = self.articles
        }
        
        if segue.identifier == "SearchSegue" {
            let search = segue.destination as! Search
            search.delegate = self
        }
        
    }
    
    @IBAction func hamburgerTapped(_ sender: UIBarButtonItem) {
        self.sideMenuController?.revealMenu()
    }
    
    @IBAction func searchTapped(_ sender: UIBarButtonItem) {
        if !self.isSearchShown {
            self.isSearchShown = true
            
            self.mainView.alpha = 0
            self.searchView.alpha = 1
        }
    }
    
    func hideSearchView() {
        if self.isSearchShown {
            self.isSearchShown = false
            
            self.mainView.alpha = 1
            self.searchView.alpha = 0
        }
    }

}
