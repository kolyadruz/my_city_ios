//
//  Search.swift
//  mycity
//
//  Created by Nikolay Druzianov on 15/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class Search: UIViewController {

    @IBOutlet weak var notFoundLbl: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    var delegate: SearchToMainContainerDelegate?
    
    var posts: [PublicationDetail]?
    var postsItemCellID = "PostsItemCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib.init(nibName: "PostsItemCell", bundle: nil), forCellWithReuseIdentifier: postsItemCellID)
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
    }

    @IBAction func cancelTapped(_ sender: UIButton) {
        self.searchBar.resignFirstResponder()
        delegate?.hideSearchView()
    }
    
}

extension Search: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        
        self.startSearch()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.posts?.removeAll()
            self.collectionView.reloadData()
        }
    }
    
    func startSearch() {
        if !(self.searchBar.text!.isEmpty) {
            let word = searchBar.text!
            self.search(word: word)
            
        } else {
            self.posts?.removeAll()
            self.collectionView.reloadData()
        }
    }
    
    func search(word: String) {
        
        Interactor().makeRequest(funcName: "search", requestParams: ["string":"\(word)"], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(SearchResponseData.self, from: responseData)
                
                self.posts = resp.data.data
                self.collectionView.reloadData()
            
                if let count = self.posts?.count {
                    if count < 1 {
                        self.notFoundLbl.isHidden = false
                        self.notFoundLbl.text = "Поиск по ключевому слову '\(resp.data.string)' не дал результатов"
                    } else {
                        self.notFoundLbl.isHidden = true
                    }
                }
                
            }
            
        }
        
    }
    
}

extension Search: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.posts?.count {
            return count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let postsItem = collectionView.dequeueReusableCell(withReuseIdentifier: postsItemCellID, for: indexPath) as! PostsItemCell
        postsItem.setCell(post: self.posts![indexPath.item])
        
        return postsItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: view.frame.width / 1.31)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let postCell = collectionView.cellForItem(at: indexPath) as? PostsItemCell {
            let postDetailView = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail") as! PostDetail
            postDetailView.postId = postCell.id
            self.present(postDetailView, animated: true, completion: nil)
        }
    }
    
}
