//
//  PostDetail.swift
//  mycity
//
//  Created by Nikolay Druzianov on 26/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class PostDetail: UIViewController {

    var postDetail: PublicationDetailExtra?
    var postId: Int?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeBtn: UIButton!
    
    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.size.width
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
        
        self.collectionView.collectionViewLayout = layout
        self.collectionView.isHidden = true
        self.getPost(self.postId!)
    }

    func getPost(_ id: Int) {
        Interactor().makeRequest(funcName: "getpost", requestParams: ["postId":"\(id)"], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(PostDetailResponseData.self, from: responseData)
                
                self.postDetail = resp.data.postData
                
                self.collectionView.reloadData()
                
            }
            
        }
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PostDetail: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.postDetail != nil {
            self.collectionView.isHidden = false
            if self.postDetail!.qrData.qrCode != ""  {
                return 3
            } else {
                return 2
            }
        } else {
            self.collectionView.isHidden = true
            return 0
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        layout.estimatedItemSize = CGSize(width: view.bounds.size.width, height: 10)
        super.traitCollectionDidChange(previousTraitCollection)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        layout.estimatedItemSize = CGSize(width: view.bounds.size.width, height: 10)
        layout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            let topCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostDetailTopCell", for: indexPath) as! PostDetailTopCell
            
            topCell.postDetail = self.postDetail
            topCell.delegate = self
            
            return topCell
        case 1:
            let descriptionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostDetailDecriptionCell", for: indexPath) as! PostDetailDecriptionCell
            
            descriptionCell.postDetail = self.postDetail
            descriptionCell.delegate = self
            
            return descriptionCell
        default:
            let qrCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostDetailQrCell", for: indexPath) as! PostDetailQrCell
            
            qrCell.postDetail = self.postDetail
            
            return qrCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.item {
        case 0:
            return CGSize(width: view.frame.width, height: view.frame.width / 1.206)
        case 1:
            return CGSize(width: view.frame.width, height: view.frame.width / 1.206)
        default:
            return CGSize(width: view.frame.width, height: view.frame.width / 1.206)
        }
        
    }
    
}
