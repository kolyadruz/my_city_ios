//
//  PostDetailDecriptionCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 26/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class PostDetailDecriptionCell: UICollectionViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    
    @IBOutlet weak var waBtn: UIButton!
    @IBOutlet weak var moreBtn: UIButton!
    
    var title = ""
    var phones: [Phone]?
    var phoneWa = ""
    var insta = ""
    
    lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    
    var delegate: UIViewController?
    
    /*override init(frame: CGRect) {
        super.init(frame: frame)
        
        //contentView.backgroundColor = UIColor.red
        //setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }*/
    
    var originalString = ""
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
    
    var postDetail: PublicationDetailExtra? {
        didSet {
            contentView.translatesAutoresizingMaskIntoConstraints = false
            
            var phoneNumbers = ""
            var instaStr = ""
            var wa = ""
            
            if let desc = postDetail?.body {
                self.descriptionLbl.attributedText = desc.htmlToAttributedString
            }
            if let phones = postDetail?.phonesCall {
                self.phones = phones
                
                phoneNumbers = "Контактные телефоны:<br />"
                
                for phone in phones {
                    phoneNumbers += "\(phone.phone)<br />"
                }
                
            }
            if let phoneWa = postDetail?.phoneWa {
                self.phoneWa = phoneWa
                
                if phoneWa != "" {
                    wa = "WhatsApp: \(phoneWa)"
                }
                
            }
            if let insta = postDetail?.instagram {
                self.insta = insta
                
                if insta != "" {
                    instaStr = "Инстаграм: http://www.instagram.com/\(insta)"
                }
            }
            if let title = postDetail?.title {
                self.title = title
            }
            
            self.originalString = "\(self.title)<br />\(self.postDetail!.body)<br /><br />\(phoneNumbers)<br />\(instaStr)<br />\(wa)".htmlToString
            
        }
    }
    
    @IBAction func waTapped(_ sender: UIButton) {
    
        
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
        
        let url  = URL(string: "whatsapp://send?text=\(escapedString!)")
        
        if UIApplication.shared.canOpenURL(url! as URL)
        {
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
        }
        
    }
    
    @IBAction func moreTapped(_ sender: UIButton) {
        
        
        if let objectsToShare = [originalString] as? [Any] {
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            delegate!.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    
}
