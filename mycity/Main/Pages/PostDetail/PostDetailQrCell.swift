//
//  PostDetailQrCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 26/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class PostDetailQrCell: UICollectionViewCell {
    
    @IBOutlet weak var qrDescriptionLbl: UILabel!
    @IBOutlet weak var qrImgView: UIImageView!
    
    lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
    
    var postDetail: PublicationDetailExtra? {
        didSet {
            contentView.translatesAutoresizingMaskIntoConstraints = false
        
            if let qr = postDetail?.qrData.qrCode {
                self.qrImgView.image = self.generateQRCode(from: qr)
            }
            if let desc = postDetail?.qrData.description {
                self.qrDescriptionLbl.text = desc
            }
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
}
