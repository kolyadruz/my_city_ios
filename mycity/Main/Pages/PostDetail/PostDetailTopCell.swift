//
//  PostDetailTopCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 26/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class PostDetailTopCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var blurredImgView: UIImageView!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    @IBOutlet weak var instaBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var writeBtn: UIButton!
    
    var delegate: UIViewController?
    
    lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    
    lazy var height: NSLayoutConstraint = {
        let height = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width / 1.206)
        height.isActive = true
        return height
    }()
    
    var postDetail: PublicationDetailExtra? {
        didSet {
            contentView.translatesAutoresizingMaskIntoConstraints = false
            
            if let image = postDetail?.image {
                self.imgView.sd_setImage(with: URL(string: image))
                self.blurredImgView.sd_setImage(with: URL(string: image))
            }
            if let favorite = postDetail?.isFavorite {
                if favorite == 1 {
                    self.likeBtn.setImage(UIImage(named: "ic_large_heart_active"), for: .normal)
                } else {
                    self.likeBtn.setImage(UIImage(named: "ic_large_heart"), for: .normal)
                }
            }
            if let title = postDetail?.title {
                self.titleLbl.text = title
            }
            if let views = postDetail?.views {
                self.viewsLbl.text = "\(views)"
            }
            
            if let insta = postDetail?.instagram {
                if insta == "" {
                    self.instaBtn.isEnabled = false
                }
            }
            
            if let phonesCall = postDetail?.phonesCall {
                if phonesCall.count == 0 {
                    self.callBtn.isEnabled = false
                    self.callBtn.imageView?.tintColor = UIColor(named: "cg_gray")
                }
            }
            
            if let phoneWa = postDetail?.phoneWa {
                if phoneWa == "" {
                    self.writeBtn.isEnabled = false
                    self.writeBtn.imageView?.tintColor = UIColor(named: "cg_gray")
                }
            }
            
        }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: targetSize.height))
    }
    
    override func draw(_ rect: CGRect) {
        
        self.callBtn.layer.cornerRadius = self.callBtn.frame.height / 2
        self.writeBtn.layer.cornerRadius = self.writeBtn.frame.height / 2
        
        self.callBtn.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.callBtn.imageEdgeInsets = UIEdgeInsets(top: 3, left: 10, bottom: 3, right: 0)
        self.writeBtn.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.writeBtn.imageEdgeInsets = UIEdgeInsets(top: 3, left: 10, bottom: 3, right: 0)
        
        self.setButtonsTargets()
        
    }
    
    func setButtonsTargets() {
        
        self.likeBtn.addTarget(self, action: #selector(likeBtnTapped), for: .touchUpInside)
        self.instaBtn.addTarget(self, action: #selector(instaTapped), for: .touchUpInside)
        self.callBtn.addTarget(self, action: #selector(callBtnTapped), for: .touchUpInside)
        self.writeBtn.addTarget(self, action: #selector(writeBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func likeBtnTapped(sender: UIButton) {
        
        var funcName = ""
        
        if self.postDetail?.isFavorite == 1 {
            funcName = "postremovefav"
        } else {
            funcName = "posttofav"
        }
        
        Interactor().makeRequest(funcName: funcName, requestParams: ["postId":"\(self.postDetail!.id)"], delegate: UIViewController()) { responseData in
            
            DispatchQueue.main.async {
                let decoder = JSONDecoder()
                print(responseData)
                let resp = try! decoder.decode(PostToFavResponseData.self, from: responseData)
                
                if resp.data.err_id == 0 {
                    if let liked = self.postDetail?.isFavorite {
                        if liked == 1 {
                            self.postDetail?.isFavorite = 0
                            self.likeBtn.setImage(UIImage(named: "ic_large_heart"), for: .normal)
                        } else {
                            self.postDetail?.isFavorite = 1
                            self.likeBtn.setImage(UIImage(named: "ic_large_heart_active"), for: .normal)
                        }
                    }
                }
            }
            
        }
    }
    
    @objc func instaTapped(sender: UIButton) {
        if let insta = self.postDetail?.instagram {
            let instagramHooks = "instagram://user?username=\(insta)"
            let instagramUrl = URL(string: instagramHooks)
            if UIApplication.shared.canOpenURL(instagramUrl!) {
                UIApplication.shared.open(instagramUrl!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.open(URL(string: "https://instagram.com/\(insta)")!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func callBtnTapped(sender: UIButton) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Позвонить", message: "Выберите номер для звонка", preferredStyle: .actionSheet)
        
        for phoneCall in self.postDetail!.phonesCall {
            var title = ""
            
            if phoneCall.description != "" {
                title = phoneCall.description
            } else {
                title = phoneCall.phone
            }
            
            let phone = UIAlertAction(title: title, style: .default) { _ in
                
                guard let number = URL(string: "tel://\(phoneCall.phone)") else { return }
                UIApplication.shared.open(number)
                
            }
            actionSheetController.addAction(phone)
            
        }
        
        let close = UIAlertAction(title: "Отмена", style: .cancel) { _ in
            
        }
        actionSheetController.addAction(close)
        
        self.delegate!.present(actionSheetController, animated: true, completion: nil)
    }
    
    @objc func writeBtnTapped(sender: UIButton) {
        if let wa = self.postDetail?.phoneWa {
            let originalString = ""
            let escapedString = originalString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
            
            let url  = URL(string: "whatsapp://send?text=\(escapedString!)&phone=\(wa)&abid=\(wa)")
            
            if UIApplication.shared.canOpenURL(url! as URL) {
                UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    
}
