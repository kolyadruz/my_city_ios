//
//  QR.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift
import AVFoundation
import QRCodeReader

class QR: UIViewController, QRCodeReaderViewControllerDelegate {

    @IBOutlet weak var savingView: UIView!
    
    @IBOutlet weak var previewView: QRCodeReaderView! {
        didSet {
            previewView.setupComponents(with: QRCodeReaderViewControllerBuilder {
                
                $0.reader = reader
                $0.showTorchButton = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton = false
                $0.showOverlayView = true
                $0.rectOfInterest = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
                
            })
        }
    }
    
    lazy var reader: QRCodeReader = QRCodeReader()
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard checkScanPermissions(), !reader.isRunning else { return }
        
        reader.didFindCode = { result in
            
            self.showFindedDialog(result: result.value)
            
        }
        
        reader.startScanning()
    }
    
    func showFindedDialog(result: String) {
        
        let alert = UIAlertController(title: "QR код", message: result, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отправить", style: .default, handler: { (_) in
            
            DispatchQueue.main.async {
                self.savingView.isHidden = false
                self.sendQR(result: result)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (_) in
            DispatchQueue.main.async {
                self.reader.startScanning()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func checkScanPermissions() -> Bool {
        
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            
            self.savingView.isHidden = true
            
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Ошибка", message: "Данное приложение не имеет разрешения на использование камеры.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Настройки", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                
            default:
                alert = UIAlertController(title: "Ошибка", message: "QR сканер не поддерживается данным устройством", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: { (_) in
                    self.reader.startScanning()
                }))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
        
    }
    
    func sendQR(result: String) {
        
        Interactor().makeQrRequest(funcName: "readqr", requestParams: ["qrCode": result], delegate: self) { responseData in
            
            DispatchQueue.main.async {
                
                self.savingView.isHidden = true
                
                let decoder = JSONDecoder()
                
                let resp = try! decoder.decode(QRData.self, from: responseData)
                
                if resp.err_id == 0 {
                    
                    let alert = UIAlertController(title: "Успешно", message: "QR код успешно считан", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "ОК", style: .cancel, handler: { (_) in
                        self.reader.startScanning()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    @IBAction func hamburgerTapped(_ sender: UIBarButtonItem) {
        self.sideMenuController?.revealMenu()
    }

}
