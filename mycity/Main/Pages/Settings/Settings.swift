//
//  Settings.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift
import FirebaseMessaging

protocol SettingsCellProtocol {
    func switchChanged(position: Int, categoryId: Int, switchedOn: Bool)
    func mainSwitchChanged(switchedOn: Bool)
}

class Settings: UIViewController, SettingsCellProtocol {

    var categories: [CategoryDetail]?
    
    var exceptedTopics = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let customFont = UIFont(name: "Montserrat-Medium", size: CGFloat(17)) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : customFont, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = UIColor(red:  23 / 255, green: 162 / 255, blue: 184 / 255 , alpha: 1.0)
        self.navigationItem.backBarButtonItem = backButtonItem
        
        if let sideMenuVC = self.sideMenuController! as? SideMenuVC {
            self.categories = sideMenuVC.data?.categories
            
            if let excepted = UserDefaults.standard.array(forKey: "exceptedTopics") {
                self.exceptedTopics = excepted as! [String]
            }
            self.tableView.reloadData()
            
            updateSwitches()
        }
    }
    
    func mainSwitchChanged(switchedOn: Bool) {
        self.exceptedTopics.removeAll()
        if !switchedOn {
            for category in self.categories! {
                if !self.exceptedTopics.contains("\(category.id)") {
                    self.exceptedTopics.append("\(category.id)")
                }
            }
        }
        
        updateSwitches()
    }
    
    func switchChanged(position: Int, categoryId: Int, switchedOn: Bool) {
        
        if !switchedOn {
            if !self.exceptedTopics.contains("\(categoryId)") {
                self.exceptedTopics.append("\(categoryId)")
            }
        } else {
            if let index = self.exceptedTopics.index(of: "\(categoryId)") {
                self.exceptedTopics.remove(at: index)
            }
        }
        
        updateSwitches()
    }

    func updateSwitches() {
        
        var allSubscribed = false
        
        for i in 1..<self.categories!.count + 1 {
            let indexPath = IndexPath(row: i, section: 0)
            if let cell = self.tableView.cellForRow(at: indexPath) as? SettingsCell {
                let categoryId = cell.category!.id
                cell.switchCtrl.setOn(!self.exceptedTopics.contains("\(categoryId)"), animated: true)
                if self.exceptedTopics.count == 0 {
                    allSubscribed = true
                }
            }
        }
        
        
        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? SettingsCell {
            cell.switchCtrl.setOn(allSubscribed, animated: true)
        }
        
        
        print(self.exceptedTopics)
        
        UserDefaults.standard.set(self.exceptedTopics, forKey: "exceptedTopics")
        
        subscribeToTopics()
    }
    
    func subscribeToTopics() {
        for category in self.categories! {
            let categoryId = "i\(category.id)"
            if !self.exceptedTopics.contains(categoryId) {
                print("subscribed to \(categoryId)")
                Messaging.messaging().subscribe(toTopic: categoryId)
            }
        }
    }
    
    @IBAction func hamburgerTapped(_ sender: UIBarButtonItem) {
        self.sideMenuController?.revealMenu()
    }
    
    
}

extension Settings: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.categories?.count {
            return count + 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        
        var category: CategoryDetail?
        
        switch indexPath.row {
        case 0:
            category = CategoryDetail.init(id: 0, image: "", name: "Получать уведомления от всех", notViewed: 0)
        default:
            category = self.categories![indexPath.row - 1]
            
        }
        cell.category = category!
        
        cell.switchCtrl.setOn(!self.exceptedTopics.contains("\(category!.id)"), animated: false)
        
        cell.delegate = self
        cell.id = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            if let currentCell = cell as? SettingsCell {
                let category  = currentCell.category!
                currentCell.switchCtrl.setOn(!self.exceptedTopics.contains("\(category.id)"), animated: false)
            }
        } else {
            if let firstCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? SettingsCell {
                if (self.exceptedTopics.count == 0) {
                    firstCell.switchCtrl.setOn(true, animated: false)
                } else {
                    if (self.exceptedTopics.count == self.categories?.count) {
                        firstCell.switchCtrl.setOn(false, animated: false)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Уведомления"
    }
    
}
