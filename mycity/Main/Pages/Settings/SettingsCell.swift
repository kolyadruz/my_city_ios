//
//  SettingsCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    var id: Int?
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var switchCtrl: UISwitch!
    
    var delegate: SettingsCellProtocol?
    
    var category: CategoryDetail? {
        didSet {
            if let title = category?.name {
                self.titleLbl.text = title
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        if self.id == 0 {
            self.delegate?.mainSwitchChanged(switchedOn: sender.isOn)
        } else {
            self.delegate?.switchChanged(position: self.id!, categoryId: self.category!.id, switchedOn: sender.isOn)
        }
    }

}
