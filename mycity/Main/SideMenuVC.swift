//
//  SideMenuVC.swift
//  mycity
//
//  Created by Nikolay Druzianov on 13/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SideMenuSwift

class SideMenuVC: SideMenuController, SideMenuControllerDelegate {

    var data: MainData?

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segue = segue as? SideMenuSegue, let identifier = segue.identifier else {
            return
        }
        
        switch identifier {
        case contentSegueID:
            segue.contentType = .content
            
            let mainNav = segue.destination as! UINavigationController
            let mainPage = mainNav.topViewController as! MainContainer
            mainPage.banners = self.data?.banners
            mainPage.categories = self.data?.categories
            mainPage.articles = self.data?.articles
        case menuSegueID:
            segue.contentType = .menu
        default:
            break
        }
    }
    
}
