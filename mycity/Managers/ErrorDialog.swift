//
//  ErrorDialog.swift
//  mycity
//
//  Created by Nikolay Druzianov on 12/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit

class ErrorDialog {
    
    func showErrorDialog(err_id: Int, viewController: UIViewController) {
        
        var err_message = ""
        
        let minSumm = UserDefaults.standard.integer(forKey: "minDiscountSum")
        
        switch err_id {
        case 1:
            err_message = "Не удалось выполнить операцию"
        case 2:
            err_message = "Категория не найдена"
        case 3:
            err_message = "Данный пост недоступен"
        case 4:
            err_message = "Заполните все поля"
        case 5:
            err_message = "Пользователь не найден"
        case 6:
            err_message = "Запись недоступна"
        case 7:
            err_message = "QR уже прочитан, либо недоступен"
        default: break
        }
        
        let alert = UIAlertController(title: "Ошибка", message: err_message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            DispatchQueue.main.async {
                
            }
        }))
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    func showErrorDialogFromQrScanner(err_id: Int, viewController: UIViewController) {
        
        var err_message = ""
        
        let minSumm = UserDefaults.standard.integer(forKey: "minDiscountSum")
        
        switch err_id {
        case 1:
            err_message = "Не удалось выполнить операцию"
        case 2:
            err_message = "Категория не найдена"
        case 3:
            err_message = "Данный пост недоступен"
        case 4:
            err_message = "Заполните все поля"
        case 5:
            err_message = "Пользователь не найден"
        case 6:
            err_message = "Запись недоступна"
        case 7:
            err_message = "QR уже прочитан, либо недоступен"
        default: break
        }
        
        let alert = UIAlertController(title: "Ошибка", message: err_message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            DispatchQueue.main.async {
                let vc = viewController as! QR
                vc.reader.startScanning()
                vc.savingView.isHidden = true
            }
        }))
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
}
