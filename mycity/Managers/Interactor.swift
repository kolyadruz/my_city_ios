//
//  Interactor.swift
//  mycity
//
//  Created by Nikolay Druzianov on 12/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import Alamofire

class Interactor {
    
    func makeRequest(funcName: String, requestParams: [String: Any], delegate: UIViewController, completion: @escaping (_ responseData: Data) -> Void) {
        
        var token = ""
        
        if let tokenInMemory = UserDefaults.standard.string(forKey: "token") {
            token = tokenInMemory
        }
        
        let headers: HTTPHeaders = [
            "Authorization":"Bearer \(token)"
        ]
        
        //print("for \(funcName) token is \(token)")
        
        request(GlobalVars.api_url + funcName, method: .post, parameters: requestParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            //print(response.response?.allHeaderFields)
            //print(response.request!.allHTTPHeaderFields)
            
            guard let statusCode = response.response?.statusCode else { return }
            
            if (200..<300).contains(statusCode) {
                
                let data = response.result.value as! NSDictionary
                let jsonObj = data["data"] as! NSDictionary
                let err_id = jsonObj["err_id"] as! Int
                
                if err_id == 0 {
                    completion(response.data!)
                    return
                } else {
                    ErrorDialog().showErrorDialog(err_id: err_id, viewController: delegate)
                }
            } else {
                ErrorDialog().showErrorDialog(err_id: 20, viewController: delegate)
            }
        }
        return
    }
    
    func makeQrRequest(funcName: String, requestParams: [String: Any], delegate: UIViewController, completion: @escaping (_ responseData: Data) -> Void) {
        
        var token = ""
        
        if let tokenInMemory = UserDefaults.standard.string(forKey: "token") {
            token = tokenInMemory
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)"
        ]
        
        request(GlobalVars.api_url + funcName, method: .post, parameters: requestParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            
            guard let statusCode = response.response?.statusCode else { return }
            
            if (200..<300).contains(statusCode) {
                
                let data = response.result.value as! NSDictionary
                let jsonObj = data["data"] as! NSDictionary
                let err_id = jsonObj["err_id"] as! Int
                
                if err_id == 0 {
                    completion(response.data!)
                    return
                } else {
                    ErrorDialog().showErrorDialogFromQrScanner(err_id: err_id, viewController: delegate)
                }
            } else {
                ErrorDialog().showErrorDialogFromQrScanner(err_id: 20, viewController: delegate)
            }
        }
        return
    }
    
}
