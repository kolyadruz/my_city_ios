//
//  ArticeToFavData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 02/07/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct ArticleToFavResponseData: Decodable {
    var data: ArticleToFavData
}

struct ArticleToFavData: Decodable {
    var err_id: Int
}
