//
//  ArticleDetailData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 29/06/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct ArticleDetailResponseData: Decodable {
    var data: ArticleDetailData
}

struct ArticleDetailData: Decodable {
    var err_id: Int
    var articleData: ArticleDetailExtra
}

struct ArticleDetailExtra: Decodable {
    
    var id: Int
    var views: Int
    var isFavorite: Int
    var datePublication: String
    var image: String
    var title: String
    var body: String
    
}
