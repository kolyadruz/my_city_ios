//
//  CategoryData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 17/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct CategoryResponseData: Decodable {
    var data: CategoryPostsData
}

struct CategoryPostsData: Decodable {
    var categoryName: String
    var err_id: Int
    var posts: [PublicationDetail]
}
