//
//  FavoritesData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct FavoritesResponseData: Decodable {
    var data: FavoritesPostsData
}

struct FavoritesPostsData: Decodable {
    var err_id: Int
    var data: [PublicationDetail]
}
