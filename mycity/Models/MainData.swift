//
//  MainData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 12/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct LoginResponseData: Decodable {
    var data: MainData
}

struct MainData: Decodable {
    var token: String
    var err_id: Int
    var banners: [PublicationDetail]
    var categories: [CategoryDetail]
    var articles: [PublicationDetail]
    var contacts: ContactsDetail
}

struct CategoryDetail: Decodable {
    
    var id: Int
    var image: String
    var name: String
    var notViewed: Int
    
}

struct ContactsDetail: Decodable {
    
    var email: String
    var emailTech: String
    var instagram: String
    var phoneCall: String
    var phoneWa: String
    
}
