//
//  NotificationData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 03/07/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct NotificationData : Decodable {
    let post: String
    let category: String
}
