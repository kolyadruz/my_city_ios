//
//  PostDetailData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 26/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct PostDetailResponseData: Decodable {
    var data: PostDetailData
}

struct PostDetailData: Decodable {
    var err_id: Int
    var postData: PublicationDetailExtra
}

struct PublicationDetailExtra: Decodable {
    
    var id: Int
    var views: Int
    var isFavorite: Int
    var datePublication: String
    var image: String
    var title: String
    var body: String
    var instagram: String
    var phoneWa: String
    var phonesCall: [Phone]
    var qrData: QRdetail
    
}

struct Phone: Decodable {
    var description: String
    var phone: String
}

struct QRdetail: Decodable {
    var description: String
    var qrCode:  String
}
