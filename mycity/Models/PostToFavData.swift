//
//  PostToFavData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 30/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct PostToFavResponseData: Decodable {
    var data: PostToFavData
}

struct PostToFavData: Decodable {
    var err_id: Int
}
