//
//  PublicationDetail.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct PublicationDetail: Decodable {
    
    var id: Int
    var image: String
    var title: String
    var views: Int
    var datePublication: String
    
}
