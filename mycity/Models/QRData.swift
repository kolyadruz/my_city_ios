//
//  QRData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 25/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct QRResponseData: Decodable {
    var data: QRData
}

struct QRData: Decodable {
    var err_id: Int
}
