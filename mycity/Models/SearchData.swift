//
//  SearchData.swift
//  mycity
//
//  Created by Nikolay Druzianov on 26/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import Foundation

struct SearchResponseData: Decodable {
    var data: SearchData
}

struct SearchData: Decodable {
    var err_id: Int
    var data: [PublicationDetail]
    var string: String
}
