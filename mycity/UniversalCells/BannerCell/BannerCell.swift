//
//  TopBannerCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 12/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import THEPageControl

class BannerCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var main: Main?
    var categoryPosts: CategoryPosts?
    var banners: [PublicationDetail]?
    
    private let cellID = "bannerItem"
    
    var velocityX = CGFloat(0.0)
    var scrollPaddings: CGFloat = 70.0
    
    var bannerWidth: CGFloat?
    var collectionViewInset: CGFloat?
    
    var timer: Timer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        bannerWidth = frame.width / 1.12
        collectionViewInset = (frame.width - bannerWidth!) / 4
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let bannersCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    let pageControl: PageControl = {
        let control = PageControl() //UIPageControl()
        
        /*// If you want to customize dots you usually create a new dot style:
        
        control.currentPage = 0
        control.pageIndicatorTintColor = UIColor.init(named: "dots_gray")
        control.currentPageIndicatorTintColor = UIColor.init(named: "dots_gray")//UIColor.purple*/
        
        control.configuration.spacing = 10
        control.translatesAutoresizingMaskIntoConstraints = false
        
        return control
    }()
    
    func setupViews() {
        addSubview(bannersCollectionView)
        //addSubview(pageControl)
        
        pageControl.backgroundColor = UIColor.red
        
        bannersCollectionView.dataSource = self
        bannersCollectionView.delegate = self
      
        bannersCollectionView.delaysContentTouches = false
        
        bannersCollectionView.register(BannerItemCell.self, forCellWithReuseIdentifier: cellID)
        bannersCollectionView.showsHorizontalScrollIndicator = false
        bannersCollectionView.showsVerticalScrollIndicator = false
    
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": bannersCollectionView]))
        //addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[v0]-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": pageControl]))
        
        //addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v0][v1(0.5)]-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": bannersCollectionView, "v1": pageControl]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": bannersCollectionView]))
        
        resetTimer()
    }
    
    func resetTimer() {
        
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
    }
    
    @objc func moveToNextPage (){
        
        let bannerWidth = self.bannersCollectionView.frame.width - self.collectionViewInset! * 2
        
        let pageWidth:CGFloat = bannerWidth + self.collectionViewInset! / 2
        let maxWidth:CGFloat = pageWidth * CGFloat(self.banners!.count)
        let contentOffset:CGFloat = self.bannersCollectionView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  round(contentOffset + pageWidth) >= round(maxWidth - pageWidth)
        {
            slideToX = 0
        }
        
        pageControlSetPage(currentOffset: slideToX)
        
        self.bannersCollectionView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannersCollectionView.frame.height), animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let banner = banners?[indexPath.item] else { return }
        if self.main != nil {
            main?.showTopBannerDetail(banner.id)
        } else if self.categoryPosts != nil {
            categoryPosts?.showTopBannerDetail(banner.id)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = banners?.count {
            
            //pageControl.numberOfPages = count
            
            pageControl.dots = Array(repeating: .customStyle, count: count)
            pageControl.center = bannersCollectionView.center
            
            print("Count of Banners: \(count)")
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! BannerItemCell
        cell.banner = banners?[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: bannerWidth!, height: bannerWidth! / 3.67)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: collectionViewInset! * 2, bottom: 0, right: collectionViewInset! * 2)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return collectionViewInset!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        scrollView.isUserInteractionEnabled = false
        self.setContentOffset(scrollView: scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollView.isUserInteractionEnabled = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else {
            return
        }
        
        self.resetTimer()
        self.setContentOffset(scrollView: scrollView)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        self.velocityX = velocity.x
        pageControlSetPage(currentOffset: targetContentOffset.pointee.x)
    
    }
    
    func pageControlSetPage(currentOffset: CGFloat) {
        
        let bannerWidth = self.bannersCollectionView.frame.width - self.collectionViewInset! * 2
        
        let pageWidth:CGFloat = bannerWidth + self.collectionViewInset! / 2
        
        let index = Int(currentOffset / pageWidth)
        
        pageControl.setActiveDotIndex(Float(index), animated: true)
        
        /*pageControl.currentPage = Int(currentOffset  / contentSize)
        
        // on each dot, call the transform of scale 1 to restore the scale of previously selected dot
        
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
        
        // transform the scale of the current subview dot, adjust the scale as required, but bigger the scale value, the downward the dots goes from its centre.
        // You can adjust the centre anchor of the selected dot to keep it in place approximately.
        
        let centreBeforeScaling = self.pageControl.subviews[self.pageControl.currentPage].center
        
        self.pageControl.subviews[self.pageControl.currentPage].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        
        /*// Reposition using autolayout
        
        self.pageControl.subviews[self.pageControl.currentPage].translatesAutoresizingMaskIntoConstraints = false*/
        
        self.pageControl.subviews[self.pageControl.currentPage].centerYAnchor.constraint(equalTo: self.pageControl.subviews[0].centerYAnchor , constant: 0)
        
        self.pageControl.subviews[self.pageControl.currentPage].centerXAnchor.constraint(equalTo: self.pageControl.subviews[0].centerXAnchor , constant: 0)
        
        self.pageControl.subviews[self.pageControl.currentPage].layer.anchorPoint = centreBeforeScaling*/
        
    }
    
    func setContentOffset(scrollView: UIScrollView) {
        
        let numOfItems = self.banners!.count
        let stopOver = scrollView.contentSize.width / CGFloat(numOfItems) - (self.collectionViewInset! * 2) / CGFloat(numOfItems)
        var x = round((scrollView.contentOffset.x + (velocityX*50)) / stopOver) * stopOver
        
        x = max(0, min(x, scrollView.contentSize.width - scrollView.frame.width))
        
        UIView.animate(withDuration: 0.12, animations: {
            scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: false)
        })
    }
    
}

extension PageControl.Dot {
    
    static var customStyle: PageControl.Dot {
        return PageControl.Dot(
            regularStyle: .init(
                radius: 3,
                fillColor: .red,
                strokeColor: .black,
                strokeWidth: 10
            ),
            activeStyle: .init(
                radius: 4,
                fillColor: .green,
                strokeColor: .black,
                strokeWidth: 5
            )
        )
    }
}

// And then you can use the new style to fill `dots` array:

//let pageControl = PageControl()
//pageControl.dots = [ .default, .customStyle, .default ]
