//
//  BannerItemCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 13/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SDWebImage

class BannerItemCell: UICollectionViewCell {
    
    var banner: PublicationDetail? {
        didSet {
            if let image = banner?.image {
                
                imageView.sd_setImage(with: URL(string: image))
                
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        return iv
    }()
    
    func setupViews() {
        addSubview(imageView)
        
        imageView.frame = CGRect(x:0, y: 0, width: frame.width, height: frame.height)
        imageView.backgroundColor = UIColor.purple
        imageView.layer.cornerRadius = frame.width / 33.5
    }
    
}
