//
//  PostsItemCell.swift
//  mycity
//
//  Created by Nikolay Druzianov on 17/05/2019.
//  Copyright © 2019 Nikolay Druzianov. All rights reserved.
//

import UIKit
import SDWebImage

class PostsItemCell: UICollectionViewCell {

    @IBOutlet weak var contentRootView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var blurredImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    
    private var shadowLayer: CAShapeLayer!
    
    var id: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(post: PublicationDetail?) {
        
        if let title = post?.title {
            self.titleLbl.text = title
        }
        
        if let views = post?.views {
            self.viewsLbl.text = "\(views)"
        }
        
        if let image = post?.image {
            self.imgView.sd_setImage(with: URL(string: image))
            self.blurredImgView.sd_setImage(with: URL(string: image))
        }
        
        if let id = post?.id {
            self.id = id
        }
        
    }
    
    override func draw(_ rect: CGRect) {
        
        self.contentRootView.layer.cornerRadius = self.contentRootView.frame.width / 23.93
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        /*if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.contentRootView.layer.cornerRadius).cgPath
            shadowLayer.fillColor = UIColor.black.cgColor
            
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2                                                                                                                                                                                                                                                                                                                                                                                                                       
            shadowLayer.shadowRadius = 3
            
            layer.insertSublayer(shadowLayer, at: 0)
        }*/
        
    }
    
}
